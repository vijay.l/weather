import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import FetchJsonData from "../components/fetchdata"
import { Provider } from "react-redux"
import store from "../components/redux/store"



const IndexPage = () => (
      <Provider store={store}>
    <Layout>
      <SEO title="Home" />
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
        {/* <Image /> */}
        <FetchJsonData/>
      </div>
    </Layout>
    </Provider>
)

export default IndexPage



