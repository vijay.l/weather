import React from "react"
import Axios from "axios"
import { connect } from "react-redux"
import { addCity } from "./redux/action"
import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
import Paper from "@material-ui/core/Paper"
import Grid from "@material-ui/core/Grid"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    background: `yellow`,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: `fit-content`,
    height: `fit-content`,
  },
}))

class FetchJsonData extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cityname: "Kolkata",
      name: "",
      weather_icons: "",
      mintemp: "",
      maxtemp: "",
      feelslike: "",
      weather_descriptions: "",
      date: "",
      time: "",
      avgtemp: "",
    }
  }

  handleChange = e => {
    e.preventDefault()
    this.setState(
      {
        [e.target.id]: e.target.value,
      },
      () => console.log(this.state)
    )
  }

  handleSubmit = e => {
    e.preventDefault()
    var city = {
      name: "",
      weather_icons: "",
      mintemp: "",
      maxtemp: "",
      feelslike: "",
      weather_descriptions: "",
      date: "",
    }
    var self = this
    Axios.get(
      `http://api.weatherstack.com/forecast?access_key=a3fd7937c2b651f30024b0cf5a7a3cfe&query=${this.state.cityname}`
    )
      .then(function (response) {
        console.log(response.data.location.name)
        var date = new Date(response.data.forecast["2020-08-25"].date)
        self.setState(
          {
            name: response.data.location.name,
            weather_icons: response.data.current.weather_icons,
            mintemp: response.data.forecast["2020-08-25"].mintemp,
            maxtemp: response.data.forecast["2020-08-25"].maxtemp,
            feelslike: response.data.current.feelslike,
            weather_descriptions: response.data.current.weather_descriptions,
            date: date.toDateString(),
            time: response.data.current.observation_time,
            avgtemp: response.data.forecast["2020-08-25"].avgtemp,
          },
          () =>
            self.props.addCity({
              name: self.state.name,
              weather_descriptions: self.state.weather_descriptions,
              weather_icons: self.state.weather_icons,
              mintemp: self.state.mintemp,
              maxtemp: self.state.maxtemp,
              feelslike: self.state.feelslike,
              date: self.state.date,
              avgtemp: self.state.avgtemp,
            })
        )

        console.log(self.props)
      })
      .catch(function (error) {
        console.log(error)
      })
    console.log(city)
    console.log(this.state)
  }

  componentDidMount() {
    Axios.get(
      `http://api.weatherstack.com/forecast?access_key=a3fd7937c2b651f30024b0cf5a7a3cfe&query=Kolkata`
    )
      .then(function (response) {
        // handle success
        console.log(response.data)
      })
      .catch(function (error) {
        // handle error
        console.log(error)
      })
      .then(function () {
        // always executed
      })
  }
  render() {
    return (
      <div>
        <form onSubmit={e => this.handleSubmit(e)}>
          <div>
            <input
              id="cityname"
              type="text"
              label="Standard"
              onChange={e => this.handleChange(e)}
              required
            />
          </div>
          <div>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              style={{ backgroundColor: "#4863A0", marginTop: `10px` }}
            >
              Search
            </Button>

            {this.state.name && (
              <div
                style={{
                  backgroundColor: "aliceblue",
                  margin: "20px 0",
                  borderRadius: "5px",
                  width: 600,
                  padding: 25,
                }}
              >
                {this.state.name && (
                  <div className={useStyles.root}>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <Paper className={useStyles.paper}>
                          {this.props.city.name}
                        </Paper>
                      </Grid>
                      <Grid item xs={12}>
                        <Paper className={useStyles.paper}>
                          {this.props.city.date} {this.state.time}
                        </Paper>
                      </Grid>
                      <Grid item xs={12}>
                        <Paper className={useStyles.paper}>
                          Day {this.props.city.maxtemp}°C↑ • Night{" "}
                          {this.props.city.mintemp}°C↓
                        </Paper>
                      </Grid>

                      <Grid item xs={6}>
                        <Paper
                          className={useStyles.paper}
                          style={{
                            height: 90,
                            fontSize: 60,
                            paddingTop: 20,
                            width: `fit-content`,
                          }}
                        >
                          {this.props.city.avgtemp}°C
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper
                          className={useStyles.paper}
                          style={{
                            width: `-webkit-fit-content`,
                            height: `-webkit-fit-content`,
                          }}
                        >
                          {" "}
                          <img
                            src={this.props.city.weather_icons}
                            style={{
                              margin: 0,
                              height: `100px`,
                              width: `100px`,
                              marginBottom: `-10px`,
                            }}
                            alt="Icon"
                          />
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={useStyles.paper}>
                          Feels Like {this.props.city.feelslike}°C
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={useStyles.paper}>
                          {this.state.weather_descriptions}
                        </Paper>
                      </Grid>
                    </Grid>
                  </div>
                )}
              </div>
            )}
          </div>
        </form>
        <div>{this.props.feelslike}</div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    city: {
      name: state.city.name,
      weather_icons: state.city.weather_icons,
      mintemp: state.city.mintemp,
      maxtemp: state.city.maxtemp,
      feelslike: state.city.feelslike,
      weather_descriptions: state.city.weather_descriptions,
      date: state.city.date,
      time: state.city.time,
      avgtemp: state.city.avgtemp,
    },
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addCity: city => dispatch(addCity(city)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FetchJsonData)
