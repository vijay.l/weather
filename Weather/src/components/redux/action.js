export const ADD_CITY = 'ADD_CITY';
// export const FILTER_BY_VALUE = 'FILTER_BY_VALUE';
// export const UPDATE_VALUE = 'UPDATE_VALUE';

export const addCity = (city) => {
    return (dispatch, getState) => {
      dispatch({ type: 'ADD_CITY', city });
    }
}

// export const filterByValue = (value) => {
//     return (dispatch, getState) => {
//         dispatch({ type: 'FILTER_BY_VALUE', value });
//       }
// }

// export const updateDiv = (value) => {
//   return (dispatch, getState) => {
//       dispatch({ type: 'UPDATE_VALUE', value });
//     }
// }
