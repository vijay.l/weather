import { ADD_CITY } from "./action"

export const initState = {
  city:{name: "",
  weather_icons: "",
  mintemp: "",
  maxtemp: "",
  feelslike: "",
  weather_descriptions: [],
  date: "",
  time: "",
  avgtemp: ""}
}

const rootReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_CITY:
        // console.log(state);
        
        console.log("adding",action)
      return {
        ...state,
        city: {name: action.city.name,
        weather_icons: action.city.weather_icons,
        mintemp: action.city.mintemp,
        maxtemp: action.city.maxtemp,
        feelslike: action.city.feelslike,
        date: action.city.date,
        weather_descriptions: action.city.weather_descriptions,
            avgtemp: action.city.avgtemp
    }
      }
    default:
      return state
  }
}

export default rootReducer
