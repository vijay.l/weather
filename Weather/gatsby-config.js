module.exports = {
  siteMetadata: {
    title: `Weather Display`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  pathPrefix: `/weather`,
  proxy: {
    prefix: "/weather",
    url: "https://vijay.l.gitlab.io/weather"
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // {
    //   resolve: "gatsby-source-graphql",
    //   options: {
    //     // Arbitrary name for the remote schema Query type
    //     typeName: "SWAPI",
    //     // Field under which the remote schema will be accessible. You'll use this in your Gatsby query
    //     fieldName: "swapi",
    //     // Url to query from
    //     url: "http://api.weatherstack.com/forecast?access_key=a3fd7937c2b651f30024b0cf5a7a3cfe&query=Mumbai",
    //     // http://api.weatherstack.com/forecast?access_key=a3fd7937c2b651f30024b0cf5a7a3cfe&query=Kolkata
    //     // headers: {
    //     //   // Learn about environment variables: https://gatsby.dev/env-vars
    //     //   access_key: `a3fd7937c2b651f30024b0cf5a7a3cfe}`,
    //     //   // query="Kolkata"
    //     // },
    //   },
    // }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
